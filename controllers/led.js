'use strict'

const net = require('net');

const TIMEOUT_START = 1000;
let TIMEOUT = TIMEOUT_START;

class LED {
    constructor() {
        this.sock = new net.Socket();
        this.LED_PORT = null;
        this.connected = false;
        this.emited = false;
        // here we store all connected clients to inform them about updates regarding the online status of led server
        this.httpClients = new Array();

        // connect handler
        this.sock.on("connect", () => {
            TIMEOUT = TIMEOUT_START;
            console.log("connected to LED server")
            this.connected = true;
            this.emitStatus();
            this.emited = false;
        });
        // error handler
        const errorHandler = () => {
            console.log("unable to connect to LED server");
            this.connected = false;
            if (!this.emited) {
                this.emitStatus();
                this.emited = true;
            }
            setTimeout(() => {
                this.sock.connect({port: this.LED_PORT});
            }, TIMEOUT);
            TIMEOUT *= 2;
            if (TIMEOUT > 64000) {
                TIMEOUT = 64000;
            }
        }
        this.sock.on("error", errorHandler);
        this.sock.on("end", errorHandler);
    }

    init(port) {
        this.LED_PORT = port
        // connect to LED control server
        console.log("connecting to LED server");
        this.sock.connect({ port });
    }
    
    // each client gets status updates for led control server
    addForStatusUpdate(res) {
        this.httpClients.push(res);
        console.log("added client for status updates");
    }
    
    removeFromStatusUpdate(res) {
        // find client in array
        const index = this.httpClients.indexOf(res);
        // client exists, remove him
        if(index != -1) {
          delete this.httpClients[index];
          return console.log("removed client from status updates");
        }
        return console.log("WARNING: unable to find client");
    }
    
    // fire status change event
    emitStatus() {
        console.log("emitting status");
        this.httpClients.forEach(client => {
          // use write to not end the stream
          client.write(`event: connection-status\n`);
          client.write(`data: ${JSON.stringify({ is_connected: this.connected })}\n\n`);
        });
    }
    
    // convert msg to JSON and send to led control server
    sendMsg(effect, effectparam = '') {
        const obj = {};
        obj.effect = effect;
        obj.effectparam = effectparam;
        const data = JSON.stringify(obj);
        // generate header with predefined length
        // a single message containing the number of bytes, padded to length = 5
        const head = data.length.toString().padEnd(5, ' ');
        this.sock.write(head + data, 'utf8');
    }
}

const LedControl = new LED();

module.exports = LedControl;
