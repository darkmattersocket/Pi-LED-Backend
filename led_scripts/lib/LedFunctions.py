#!/usr/bin/python3
# coding=utf-8

import Adafruit_GPIO.SPI as SPI
import logging
import math
import queue
import threading
import time

import lib.WS2801 as WS2801

SPI_PORT = 0
SPI_DEVICE = 0
PIXEL_COUNT = 147
PIXEL = WS2801.WS2801Pixels(PIXEL_COUNT, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

logger = logging.getLogger("LedServer.Functions")


class Balls:
    GRAVITY = -9.81              # Downward (negative) acceleration of gravity in m/s^2
    h0 = 1                  # Starting height, in meters, of the ball (strip length)
    vImpact0 = math.sqrt(-2 * GRAVITY * h0)      # Impact velocity of the ball when it hits the ground if "dropped" from the top of the strip
    BALL_COLORS = [
        [255, 255, 255], [255, 0, 0], [0, 255, 0], [0, 0, 255], [255, 255, 0],
        [0, 255, 0], [0, 255, 255], [0, 0, 255], [255, 0, 255], [255, 0, 0]
    ]
    BC_LEN = len(BALL_COLORS)

    def init_balls(self, n):
        self.NUM_BALLS = n
        self.h = [0] * self.NUM_BALLS                             # An array of heights
        self.vImpact = [0] * self.NUM_BALLS                       # As time goes on the impact velocity will change, so make an array to store those values
        self.tCycle = [0] * self.NUM_BALLS                       # The time since the last time the ball struck the ground
        self.pos = [0] * self.NUM_BALLS                       # The integer position of the dot on the strip (LED index)
        self.tLast = [0] * self.NUM_BALLS                       # The clock time of the last ground strike
        self.COR = [0] * self.NUM_BALLS                       # Coefficient of Restitution (bounce damping)

        for i in range(self.NUM_BALLS):
            self.tLast[i] = time.time() / (1000)
            self.h[i] = self.h0
            self.pos[i] = 0                             # Balls start on the ground
            self.vImpact[i] = self.vImpact0                  # And "pop" up at vImpact0
            self.tCycle[i] = 0
            self.COR[i] = 0.90 - (float(i) / self.NUM_BALLS ** 2)

    def bounce(self, kill_switch: threading.Event, bounce_queue: queue.Queue):
        PIXEL.clear()
        self.init_balls(5)
        while True:

            if kill_switch.is_set():
                logger.debug("bouncing balls stopped")
                return

            for i in range(self.NUM_BALLS):
                self.tCycle[i] = (time.time_ns() / (1000 * 1000)) - self.tLast[i]     # Calculate the time since the last time the ball was on the ground

                # A little kinematics equation calculates positon as a function of time, acceleration (gravity) and intial velocity
                self.h[i] = 0.5 * self.GRAVITY * pow((self.tCycle[i] / 1000), 2.0) + (self.vImpact[i] * (self.tCycle[i] / 1000))

                if self.h[i] < 0:
                    self.h[i] = 0                            # If the ball crossed the threshold of the "ground," put it back on the ground
                    self.vImpact[i] = self.COR[i] * self.vImpact[i]   # and recalculate its new upward velocity as it's old velocity * COR
                    self.tLast[i] = time.time_ns() / (1000 * 1000)

                    if self.vImpact[i] < 0.01:
                        self.vImpact[i] = self.vImpact0          # If the ball is barely moving, "pop" it back up at vImpact0
                self.pos[i] = round(self.h[i] * (PIXEL_COUNT - 1) / self.h0)       # Map "h" to a "pos" integer index position on the LED strip
                # print(pos[i])

            # Choose color of LEDs, then the "pos" LED on
            for i in range(self.NUM_BALLS):
                PIXEL.set_pixel_rgb(self.pos[i], self.BALL_COLORS[i % self.BC_LEN][0], self.BALL_COLORS[i % self.BC_LEN][1], self.BALL_COLORS[i % self.BC_LEN][2])

            PIXEL.show()

            # Then off for the next loop around
            for i in range(self.NUM_BALLS):
                PIXEL.set_pixel_rgb(self.pos[i], 0, 0, 0)

            # clear the queue since the script is not fast enough to react to quick changes in speed
            while not bounce_queue.empty():
                ball_count = bounce_queue.get(block=False)
                logger.debug(f"new balls: {ball_count}")
                self.init_balls(ball_count)


def setColor(color: str):
    # color is hex string
    PIXEL.set_pixels(int(color, 16))
    PIXEL.show()
    logger.debug("pixel set")


def wheel(pos: int):
    if pos < 85:
        return WS2801.RGB_to_color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return WS2801.RGB_to_color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return WS2801.RGB_to_color(0, pos * 3, 255 - pos * 3)


def rainbow(kill_switch: threading.Event, rainbow_queue: queue.Queue):
    logger.debug("rainbow started")
    SLEEPTIME = 0.05
    PIXEL.clear()
    while True:
        for j in range(256):  # one cycle of all 256 colors in the wheel

            # check if we need to stop
            if kill_switch.is_set():
                logger.debug("rainbow stopped")
                return

            for i in range(PIXEL.count()):
                PIXEL.set_pixel(i, wheel(((i * 256 // PIXEL_COUNT) + j) % 256))
            PIXEL.show()

            # get current sleeptime
            # clear the queue since the script is not fast enough to react to quick changes in speed
            while not rainbow_queue.empty():
                message = rainbow_queue.get(block=False)

                # mapping speed value from [1-100] to [0.001 - 1]
                SLEEPTIME = round((101 - message) / ((10 * message) + (91 - message)), 3)
                logger.debug(f"new sleeptime: {SLEEPTIME}")
            time.sleep(SLEEPTIME)

# 30 min --> 30*60 = 1.800
# 1.800 / 255 = 7 secs Delay or Sleeptime between brightness increase


def sunrise(kill_switch: threading.Event, DELAY: float):
    logger.debug("Sunrise started")
    PIXEL.clear()
    SLEEP = 0.001

    # helligkeit von 0 auf 255
    for j in range(256):  # one cycle of all 256 colors in the wheel
        for i in range(PIXEL.count()):
            r = j
            g = int(j / 2.5)
            b = int(j / 6)
            PIXEL.set_pixel_rgb(i, r, b, g)
        PIXEL.show()

        t0 = 0
        while t0 < DELAY:
            if kill_switch.is_set():
                logger.debug("sunrise stopped")
                return

            t0 = t0 + SLEEP
            time.sleep(SLEEP)

        logger.debug("R: %i, B: %i, G: %i" % (r, b, g))
    logger.debug("Moinsen")
