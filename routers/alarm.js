const express = require("express");
const controllers = require("../controllers/alarm");

const router = express.Router();

router.get("/", controllers.getAlarms);
router.get("/:id", controllers.getAlarmById);

router.post("/", controllers.createAlarm);
router.put("/:id", controllers.updateAlarm);
router.delete("/:id", controllers.deleteAlarm);

module.exports = router;
