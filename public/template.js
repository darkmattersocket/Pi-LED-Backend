'use strict'

class AlarmCard extends HTMLElement {
    constructor() {
        super();
        const shadowRoot = this.attachShadow({ mode: "open"});

        const template = document.getElementById("alarm-template");
        const content = template.content.cloneNode(true);

        shadowRoot.appendChild(content);
        this.__setupNameArea();
        this.__setupRepeatArea();
        this.__setupEffectArea();
        this.__setupButtonArea();
        this.__setupChangeableElements();
    }
    // private
    __setupNameArea() {
        const input = this.shadowRoot.getElementById("name-input");
        const label = this.shadowRoot.getElementById("name-input-label");
        const button = this.shadowRoot.getElementById("name-input-button");

        // hide button after user clicks on it, use label to show name
        button.addEventListener("click", () => {
            // set label
            label.innerText = this.getName;
            // hide input and button
            label.classList.remove("edit");
            button.classList.remove("edit");
            input.classList.remove("edit");
        });

        // show edit controls
        label.addEventListener("click", () => {
            label.classList.add("edit");
            button.classList.add("edit");
            input.classList.add("edit");
            input.focus();
        });

        // make pressing enter while typing in the name the same as clicking the ok button
        input.addEventListener("keydown", event => {
            if(event.code === "Enter") {
                button.click();
            }
        })
    }
    __setupRepeatArea() {
        const labels = this.shadowRoot.querySelectorAll(".repeat-input-container label");
        const days_container = this.shadowRoot.querySelector(".days-input-container");
        const repeat_label = this.shadowRoot.getElementById("repeat-input-label");

        // toggle visibility of day checkboxes when user toggles repeat checkbox
        repeat_label.addEventListener("click", () => {
            days_container.classList.toggle("visible");
        });

        // set or remove class for css styling
        for(let i = 0; i < labels.length; i++) {
            labels[i].addEventListener("click", this.__markLabel);
        }
    }
    __setupEffectArea() {
        const effect = this.shadowRoot.getElementById("effect-input");
        const effect_options = this.shadowRoot.querySelectorAll(".effect-options-container > div");
        // show effect options when needed
        effect.addEventListener("input", () => {
            // remove visibility of all options
            effect_options.forEach(opt => {
                opt.classList.remove("visible");
            });
            // show options only when needed
            if(effect.value === "Sunrise") {
                effect_options[0].classList.add("visible");
            }
            else if(effect.value === "Color") {
                effect_options[1].classList.add("visible");
            }
            else if(effect.value === "Rainbow") {
                effect_options[2].classList.add("visible");
            }
            else if(effect.value === "Bouncing Balls") {
                effect_options[3].classList.add("visible");
            }
        });
    }
    __setupButtonArea() {
        const delete_alarm_button = this.shadowRoot.getElementById("delete-alarm");
        const save_changes_button = this.shadowRoot.getElementById("save-alarm");
        // hide by default
        save_changes_button.className = "hidden";

        // destroy alarm on button click
        delete_alarm_button.addEventListener("click", () => {
            // send delete request if not local alarm
            if(this.getId != 0) {
                const destination = `/alarm/${this.getId}`;
                const request = new XMLHttpRequest();
                request.open("DELETE", destination);
                request.send();
            }
            // Seppuku
            this.shadowRoot.host.remove();
        });
        // bind this to function
        this.btn_handler = this.__sendAlarm.bind(this);
        save_changes_button.addEventListener("click", this.btn_handler);
    }
    __markLabel() {
        this.classList.toggle("checked");
    }
    __setupChangeableElements() {
        // setup event listener so whenever a user changes any alarm settings
        // he enters the edit mode where we show him the "save your changes" button
        const changeable_element = this.shadowRoot.querySelectorAll(".changeable-element");
        const save_changes_button = this.shadowRoot.getElementById("save-alarm");
        changeable_element.forEach(element => {
            element.addEventListener("change", () => {
                // clear timeout if exists
                if(this.btn_timeout) {
                    clearTimeout(this.btn_timeout);
                    this.btn_timeout = false;
                    // revert button to default state
                    save_changes_button.addEventListener("click", this.btn_handler);
                    save_changes_button.innerText = "Save";
                }
                if(this.__isEqual(this.alarm_state)) {
                    save_changes_button.className = "hidden";
                }
                else {
                    save_changes_button.className = "";
                }
            });
        });
    }

    __makeObject() {
        // we´re going to stringify this object
        const alarm = {};

        // alarm.id = this.getId;
        alarm.name = this.getName;
        alarm.type = this.getType;
        alarm.time = this.getTime;
        alarm.days = this.getDays;
        alarm.effect = this.getEffect;
        alarm.effectparam = this.getEffectParam;
        alarm.enabled = this.getEnable;

        return alarm;
    }
    __sendAlarm() {
        const that = this;
        const save_changes_button = that.shadowRoot.getElementById("save-alarm");

        // prepare the request
        const destination = "/alarm";
        const id = this.getId;
        // const alarm = JSON.stringify(this.__makeObject());
        const alarm = this.__makeObject();
        const request = new XMLHttpRequest();

        request.open("PUT", destination + "/" + id);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.onreadystatechange = responseHandler
        request.send(JSON.stringify(alarm));
        console.log(alarm);

        function responseHandler() {
            if(this.readyState === 4 && this.status === 200) {
                // if saving was successful, disable click
                that.alarm_state = that.__makeObject();
                save_changes_button.removeEventListener("click", that.btn_handler);
                save_changes_button.className = "accepted";
                save_changes_button.innerText = "Saved!"
                that.btn_timeout = setTimeout(() => {
                    // revert button to default state
                    save_changes_button.addEventListener("click", that.btn_handler);
                    save_changes_button.className = "hidden";
                    save_changes_button.innerText = "Save";
                }, 5 * 1000);
            }
            else if(this.readyState === 4 && this.status !== 200) {
                console.log("something went wrong");
                console.log(this.status);
            }
        }
    }
    __isEqual(alarm) {
        const current = this.__makeObject();
        return (JSON.stringify(current) === JSON.stringify(alarm)) ? 1 : 0;
    }

    // public
    initializeAlarm(alarm) {
        this.setTime = alarm.time;
        this.setName = alarm.name;
        this.setId = alarm.id;
        this.setType = alarm.type;
        this.setDays = alarm.days;
        this.setEffect = alarm.effect;
        this.setEffectParam = alarm.effectparam;
        this.setEnable = alarm.enabled;
        // save alarm state for comparison
        this.alarm_state = this.__makeObject();
        // hide 'save changes' button because due to our set calls it got activated
        this.shadowRoot.getElementById("save-alarm").className = "hidden";
    }
    get getName() {
        return this.shadowRoot.getElementById("name-input").value;
    }
    get getId() {
        const id = this.shadowRoot.querySelector(".alarm-container").getAttribute("ident");
        return (id === null ? 0 : Number(id));
    }
    get getTime() {
        return this.shadowRoot.getElementById("time-input").value;
    }
    get getType() {
        return Number(this.shadowRoot.querySelector(".alarm-container").getAttribute("alarm_type"));
    }
    get getDays() {
        // calculate value when repeat input is checked
        const repeat_checkbox = this.shadowRoot.getElementById("repeat-input");

        // no need to calculate anything
        if(!repeat_checkbox.checked) {
            return 0;
        }
        // days value is 8bit int, each bit representing a weekday, 5 --> 00000101 --> Wednesday, Monday
        // monday is LSB
        const days = this.shadowRoot.querySelectorAll(".days-input-container input[type='checkbox']");
        let days_value = 0;
        for(let i = 0; i < days.length; i++) {
            if(days[i].checked) {
                days_value += 2 ** i;
            }
        }
        return days_value;
    }
    get getEffect() {
        return this.shadowRoot.getElementById("effect-input").value;
    }
    get getEffectParam() {
        const effect = this.shadowRoot.getElementById("effect-input").value;
        if(effect === "Rainbow") {
            const slider_value = this.shadowRoot.getElementById("speed-input").value;
            return slider_value;
        }
        else if(effect === "Color") {
            const color = this.shadowRoot.getElementById("color-input");
            // remove leading #, '#ff0000' --> 'ff0000'
            return color.value.slice(1);
        }
        else if(effect === "Bouncing Balls") {
            const balls = this.shadowRoot.getElementById("balls-input").value;
            return balls;
        }
        else if(effect ==="Sunrise") {
            const sunrise_speed = this.shadowRoot.getElementById("sunrise-input").value;
            return sunrise_speed;
        }
        else {
            return "";
        }
    }
    get getEnable() {
        const enable_checkbox = this.shadowRoot.getElementById("enable-input");
        return enable_checkbox.checked;
    }

    set setName(NAME){
        this.shadowRoot.getElementById("name-input").value = NAME;
        this.shadowRoot.getElementById("name-input-label").innerText = NAME;
    }
    set setId(ID) {
        this.shadowRoot.querySelector(".alarm-container").setAttribute("ident", ID);
    }
    set setType(TYPE) {
        this.shadowRoot.querySelector(".alarm-container").setAttribute("alarm_type", TYPE);
    }
    set setTime(TIME) {
        this.shadowRoot.getElementById("time-input").value = TIME;
    }
    set setDays(DAYS) {
        // days value is 8bit int, each bit representing a weekday, 5 --> 00000101 --> Wednesday, Monday
        // Monday is LSB
        const days_value = DAYS;
        if(days_value === 0) {
            // use default setting
            return;
        }

        const day_labels = this.shadowRoot.querySelectorAll(".days-input-container label");
        const repeat_button = this.shadowRoot.getElementById("repeat-input-label");

        for(let i = 0; i < 7; i++) {
            // shift to the right and only look at LSB
            let day = (days_value >> i) & 1;
            if(day) {
                // activate the checkbox
                day_labels[i].dispatchEvent(new MouseEvent("click"));
            }
        }
        // activate the main checkbox and show day checkboxes container
        repeat_button.dispatchEvent(new MouseEvent("click"));
    }
    set setEffect(EFFECT) {
        const effect = this.shadowRoot.getElementById("effect-input")
        effect.value = EFFECT;
        // trigger event handler to show correct effect options
        effect.dispatchEvent(new InputEvent("input"));
    }
    set setEffectParam(EFFECT_PARAM) {
        const effect = this.shadowRoot.getElementById("effect-input").value;
        const slider = this.shadowRoot.getElementById("speed-input");
        const color = this.shadowRoot.getElementById("color-input");
        const balls = this.shadowRoot.getElementById("balls-input");
        const sunrise = this.shadowRoot.getElementById("sunrise-input");

        if(effect === "Rainbow") {
            slider.value = EFFECT_PARAM;
        }
        else if(effect === "Color") {
            // add hashtag before value, wont work otherwise
            color.value = `#${EFFECT_PARAM}`;
        }
        else if(effect === "Bouncing Balls") {
            balls.value = EFFECT_PARAM;
        }
        else if(effect === "Sunrise") {
            sunrise.value = EFFECT_PARAM;
        }
    }
    set setEnable(ENABLE) {
        if(!ENABLE) return;

        const enable_label = this.shadowRoot.getElementById("enable-label");
        enable_label.dispatchEvent(new MouseEvent("click"));
    }
}
customElements.define("alarm-template", AlarmCard);
